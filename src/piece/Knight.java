package piece;

import chess.Chess;

/**
 * Represents a Knight piece
 * @author Daanyal and Ateeb
 */
public class Knight extends Piece {

    /**
     * Creates a Knight piecec with specified row, column and team
     * @param r row
     * @param c column
     * @param t team
     */
    public Knight(int r, int c, String t){
        super(r, c, t);
    }

    /**
     * Creates string representation of knight piece
     * @return string version of black or white knight
     */
    public String toString(){
        if(team == Team.BLACK)
            return "bN";
        else
            return "wN";
    }

    /**
     * tell if desired position would be a legal move for a knight piece
     * @param psn desired position
     * @return whether the move is legal or not
     */
    public boolean isLegalMove(String psn) {
        int r = Chess.getRow(psn);
        int c = Chess.getCol(psn);
        boolean isValid = false;

        if(Chess.board[r][c] != null && this.getTeam() == Chess.board[r][c].getTeam()){
            return false;
        }

        if (r == row+2 || r == row-2){
            if (c == col +1 || c == col -1){
                return true;
            }
            return false;
        }
        else if (c == col+2 || c == col-2){
            if (r == row +1 || r == row -1){
                return true;
            }
            return false;
        }
        else{
            return false;
        }
    }
}
