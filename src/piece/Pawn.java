package piece;

import chess.Chess;

/**
 * @author Syed Ateeb Jamal and Daanyal Farooqi
 *
 * A class to represent the Pawn piece in Chess. Inherits from Piece.
 */
public class Pawn extends Piece {

    private boolean hasMoved;
    /**
     * Defines which pawn is currently able to be taken in an en passant move. A static variable.
     */
    public static Pawn passant;

    /**
     * Creates a new Pawn with the specified row, column, and team.
     * @param r an int of the row index
     * @param c an int of the column index
     * @param t A string to represent the team - Black or White.
     */
    public Pawn(int r, int c, String t){
        super(r, c, t);
        hasMoved = false;
        passant = null;
    }

    /**
     * Returns the current piece as a string to be represented in the UI command line.
     *
     * @return Either "bp" or "wp" based on the team.
     */
    public String toString(){
        if(team == Team.BLACK)
            return "bp";
        else
            return "wp";
    }

    /**
     * Determines if moving to the desired location is a valid move.
     * @param psn The new desired position of the piece.
     * @return Whether moving to the desired location is true or false.
     */
    public boolean isLegalMove(String psn){
        int r = Chess.getRow(psn);
        int c = Chess.getCol(psn);
        boolean isValid = false;

        if(Chess.board[r][c] != null && this.getTeam() == Chess.board[r][c].getTeam()){
            return false;
        }

        //Check for passant move
        if(passant != null && Chess.board[r][c] == null){
            if(row == passant.getRow() && (col == passant.getCol() + 1 || col == passant.getCol() - 1)){
                if(c == passant.getCol()){
                    if(this.team == Team.WHITE && r == passant.getRow() - 1){
                        Chess.board[passant.getRow()][passant.getCol()] = null;
                        passant = null;
                        return true;
                    }
                    else if(this.team == Team.BLACK && r == passant.getRow() + 1){
                        Chess.board[passant.getRow()][passant.getCol()] = null;
                        passant = null;
                        return true;
                    }
                }
            }
        }

        //forward move (check for first turn)
        if(col == c){
            if(team == Team.WHITE){
                if(r == row - 1 || (!hasMoved && r == row - 2)){
                    if(r == row - 2){
                        if(Chess.board[row - 1][c] == null && Chess.board[r][c] == null){
                            passant  = this;
                            isValid = true;
                        }
                        else{
                            isValid = false;
                        }
                    }
                    else {
                        if(Chess.board[r][c] == null) {
                            isValid = true;
                        }
                        else {
                            isValid = false;
                        }
                    }
                }
            }
            else{
                if(r == row + 1 || (!hasMoved && r == row + 2)){
                    if(r == row + 2){
                        if(Chess.board[row + 1][c] == null && Chess.board[r][c] == null) {
                            passant = this;
                            isValid = true;
                        }
                        else {
                            isValid = false;
                        }
                    }
                    else{
                        if(Chess.board[r][c] == null) {
                            isValid = true;
                        }
                        else {
                            isValid = false;
                        }
                    }
                }
            }
        }
        //else check for capture
        else{
            if(team == Team.WHITE){
                if(r == row - 1 && (c == col + 1 || c == col - 1)){
                    if(Chess.board[r][c] != null){
                        isValid = true;
                    }
                }
            }
            else{
                if(r == row + 1 && (c == col + 1 || c == col -1)){
                    if(Chess.board[r][c] != null){
                        isValid = true;
                    }
                }
            }
        }

        if(isValid){
            hasMoved = true;
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Checks whether the Pawn has moved.
     * @return whether the pawn has moved.
     */
    public boolean hasMoved() {
        return hasMoved;
    }

}
