package piece;

import chess.Chess;

/**
 * Represents a Rook piece
 * @author Daanyal and Ateeb
 */
public class Rook extends Piece {
    public boolean hasMoved;
    public Rook(int r, int c, String t){
        super(r, c, t);
        hasMoved = false;
    }

    /**
     * Creates string repersentation of a Rook piece
     * @return string version of a black or white rook piece
     */
    public String toString(){
        if(team == Team.BLACK)
            return "bR";
        else
            return "wR";
    }

    /**
     * Tell whether not a desired position is a legal move of a rook piece
     * @param psn desired position
     * @return whether or not move is legal 
     */
    public boolean isLegalMove(String psn) {
        int r = Chess.getRow(psn);
        int c = Chess.getCol(psn);

        // check if space is already occupied by piece of the same team
        if(Chess.board[r][c] != null && this.getTeam() == Chess.board[r][c].getTeam()){
            return false;
        }

        int cc;
        int rr;
        // check if it is a horizantally valid move
        if (r == row){
            if (c > col){
                for (cc = col + 1; cc < c; cc++ ){
                    if(Chess.board[r][cc] != null){
                        return false;
                    }
                }
                this.hasMoved = true;
                return true;
            }
            else{
                for (cc = col -1; cc > c; cc--){
                    if(Chess.board[r][cc] != null){
                        return false;
                    }
                }
                this.hasMoved = true;
                return true;
            }
        }
        //check if it is a vertically valid move
        else if (c == col){
            if (r > row){
                for (rr = row + 1; rr < r; rr++ ){
                    if(Chess.board[rr][c] != null){
                        return false;
                    }
                }
                this.hasMoved = true;
                return true;
            }
            else{
                for (rr = row -1; rr > r; rr--){
                    if(Chess.board[rr][c] != null){
                        return false;
                    }
                }
                this.hasMoved = true;
                return true;
            }
        }
        else {
            return false;
        }
    }

}
