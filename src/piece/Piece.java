package piece;

import chess.Chess;

import java.util.ArrayList;

/**
 * A super class from which all chess pieces will inherit. Creates the structure of positon, team, defines some standard methods and abstract methods.
 * @author Syed Ateeb Jamal and Daanyal Farooqi
 */
public abstract class Piece {

    /** An enumerated type which restricts team to be either BLACK or WHITE
     */
    public enum Team { BLACK, WHITE };

    /**
     * Holds the row index of a piece.
     */
    public int row;

    /**
     * Holds the column index of a piece.
     */
    public int col;

    /**
     * Holds the team of the piece as an enumerated type.
     */
    public Team team;

    /**
     * Creates a piece based on the specified row, column, and team.
     *
     * @param r the row index of a new piece
     * @param c the column index of a new piece
     * @param t the team as a string for a new piece
     */
    public Piece(int r, int c, String t){
        row = r;
        col = c;

        if(t.equalsIgnoreCase("BLACK")){
            team = Team.BLACK;
        }
        else{   //default value
            team = Team.WHITE;
        }
    }

    /**
     * An abstract method to ensure all chess pieces define a legal move.
     * @param psn The new desired position of the piece.
     * @return Whether or not the desired move is legal
     */
    public abstract boolean isLegalMove(String psn);

    /**
     * Creates a list of all the possible moves a piece can make.
     * @return An ArrayList of all the possible moves for the piece.
     */
    public ArrayList<String> allPossibleMoves(){
        ArrayList<String> arr = new ArrayList<String>();
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++) {
                if (this.isLegalMove(this.psnString(i,j))) {
                    arr.add(this.psnString(i,j));
                }
            }
        }
        return arr;
    }

    /**
     * Returns the current position in the format of the UI string.
     * @return current position of a piece in a UI string
     */
    public String psnString (){
        String ret = "";

        switch(col){
            case 0: ret += "a"; break;
            case 1: ret += "b"; break;
            case 2: ret += "c"; break;
            case 3: ret += "d"; break;
            case 4: ret += "e"; break;
            case 5: ret += "f"; break;
            case 6: ret += "g"; break;
            default: ret += "h";
        }

        ret += 8 - row;

        return ret;
    }

    /**
     * Returns the current position of a piece with the specified position.
     * @param r the row index of the current piece
     * @param c the column index of the current piece
     * @return The piece's position as a UI string.
     */
    public static String psnString(int r, int c){
            String ret = "";

            switch(c){
                case 0: ret += "a"; break;
                case 1: ret += "b"; break;
                case 2: ret += "c"; break;
                case 3: ret += "d"; break;
                case 4: ret += "e"; break;
                case 5: ret += "f"; break;
                case 6: ret += "g"; break;
                default: ret += "h";
            }

            ret += 8 - r;

            return ret;
        }

    /**
     * Gets the current row index
      * @return An int of the current row index.
     */
    public int getRow() {
        return row;
    }

    /**
     * Sets the current row index.
     * @param row an int of the new row index
     */
    public void setRow(int row) {
        this.row = row;
    }

    /**
     * Gets the current column index
     * @return an int of the current column index
     */
    public int getCol() {
        return col;
    }

    /**
     * Sets the current column index.
     * @param col an int of the new column index.
     */
    public void setCol(int col) {
        this.col = col;
    }

    /**
     * Gets the team assignment
     * @return The team of the piece as an enumerated type
     */
    public Team getTeam() {
        return team;
    }
}
