package piece;

import chess.Chess;

/**
 * Represents a Queen piece
 * @author Daanyal and Ateeb
 */
public class Queen extends Piece {

    /**
     * Creates a queen piece with specified row, column and team
     * @param r row
     * @param c column
     * @param t team
     */
    public Queen(int r, int c, String t){
        super(r, c, t);
    }

    /**
     * Creates string representation of a queen piece
     * @return string version of black or white queen piece
     */
    public String toString(){
        if(team == Team.BLACK)
            return "bQ";
        else
            return "wQ";
    }

    /**
     * Tells whethers or not desired position is a legal move for a Queen piece
     * @param psn desired position
     * @return whether move is legal or not
     */
    public boolean isLegalMove(String psn) {
        int r = Chess.getRow(psn);
        int c = Chess.getCol(psn);
        boolean isValid = false;
        // check if space is already occupied by piece of the same team
        if(Chess.board[r][c] != null && this.getTeam() == Chess.board[r][c].getTeam()){
            return false;
        }

        int cc;
        int rr;
        //check if its a diagonally valid move
        if (Math.abs(r - row) == Math.abs(c - col)){
            if (r > row && c > col){
                for (int d = 1; d < r - row; d++){
                    if (Chess.board[row+d][col+d] != null){
                        return false;
                    }
                }
                return true;
            }
            else if (r < row && c > col){
                for (int d = 1; d < c - col; d++){
                    if (Chess.board[row-d][col+d] != null){
                        return false;
                    }
                }
                return true;
            }
            else if (r > row && c < col){
                for (int d = 1; d < r - row; d++){
                    if (Chess.board[row+d][col-d] != null){
                        return false;
                    }
                }
                return true;
            }
            else if (r < row && c < col){
                for (int d = 1; d < row - r; d++){
                    if (Chess.board[row-d][col-d] != null){
                        return false;
                    }
                }
                return true;
            }
        }
        // check if it is a horizantally valid move
        else if (r == row){
            if (c > col){
                for (cc = col + 1; cc < c; cc++ ){
                    if(Chess.board[r][cc] != null){
                        return false;
                    }
                }
                return true;
            }
            else{
                for (cc = col -1; cc > c; cc--){
                    if(Chess.board[r][cc] != null){
                        return false;
                    }
                }
                return true;
            }
        }
        //check if it is a vertically valid move
        else if (c == col){
            if (r > row){
                for (rr = row + 1; rr < r; rr++ ){
                    if(Chess.board[rr][c] != null){
                        return false;
                    }
                }
            }
            else{
                for (rr = row -1; rr > r; rr--){
                    if(Chess.board[rr][c] != null){
                        return false;
                    }
                }
            }
            return true;
        }
        else {
            return false;
        }
    return false;
    }
}
