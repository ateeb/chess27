package piece;

import chess.Chess;

/**
 * Represents a Bishop piece
 * @author Daanyal and Ateeb
 */
public class Bishop extends Piece {

    /**
     * Creates a bishop with a specified row, column and team
     * @param r row
     * @param c column
     * @param t team
     */
    public Bishop(int r, int c, String t){
        super(r, c, t);
    }
    /**
     * Creates string representation of Bishop piece
     * @return String version of white or black bishop
     */
    public String toString(){
        if(team == Team.BLACK)
            return "bB";
        else
            return "wB";
    }

    /**
     * Tell if a given specified position would be a legal move for a bishop piece
     * @param psn desired position
     * @return Whether the move is legal or not
     */
    public boolean isLegalMove(String psn) {
        int r = Chess.getRow(psn);
        int c = Chess.getCol(psn);
        boolean isValid = false;

        // check if space is already occupied by piece of the same team
        if(Chess.board[r][c] != null && this.getTeam() == Chess.board[r][c].getTeam()){
            return false;
        }
        // check if it is on a valid diagonal path
        if (Math.abs(r - row) != Math.abs(c - col)){
            return false;
        }
        //check if there are any pieces blocking the diagonal path
        if (r > row && c > col){
            for (int d = 1; d < r - row; d++){
                if (Chess.board[row+d][col+d] != null){
                    return false;
                }
            }
            return true;
        }
        else if (r < row && c > col){
            for (int d = 1; d < c - col; d++){
                if (Chess.board[row-d][col+d] != null){
                    return false;
                }
            }
            return true;
        }
        else if (r > row && c < col){
            for (int d = 1; d < r - row; d++){
                if (Chess.board[row+d][col-d] != null){
                    return false;
                }
            }
            return true;
        }
        else if (r < row && c < col){
            for (int d = 1; d < row - r; d++){
                if (Chess.board[row-d][col-d] != null){
                    return false;
                }
            }
            return true;
        }
        else{
            return false;
        }
    }

}
