package piece;

import chess.Chess;

/**
 * Represents a King piece
 * @author Daanyal and Ateeb
 */
public class King extends Piece {
    private boolean hasMoved;

    /**
     * Creates a King with specified row, column and team
     * @param r row
     * @param c column
     * @param t team
     */
    public King(int r, int c, String t){
        super(r, c, t);
        hasMoved = false;
    }

    /**
     * Creates string representation of King piece
     * @return String version of Black or White king
     */
    public String toString(){
        if(team == Team.BLACK)
            return "bK";
        else
            return "wK";
    }

    /**
     * Tells if a given position would be a legal move for a king piece
     * @param psn desired position
     * @return Whether the move is legal or not
     */
    public boolean isLegalMove(String psn) {
        int r = Chess.getRow(psn);
        int c = Chess.getCol(psn);
        boolean isValid = false;

        // check for castling
        if (!(this.hasMoved) && r == row){
            if (c == 6 && Chess.board[r][7] instanceof Rook && !((Rook)Chess.board[r][7]).hasMoved){
                if (Chess.board[r][5] == null && Chess.board[r][6] == null){
                    Chess.board[r][5] = Chess.board[r][7];
                    Chess.board[r][7] = null;

                    Chess.board[r][5].setRow(r);
                    Chess.board[r][5].setCol(5);

                    this.hasMoved = true;
                    return true;
                }

            }
            else if (c == 2 && Chess.board[r][0] instanceof Rook && !((Rook)Chess.board[r][0]).hasMoved ){
                if (Chess.board[r][1] == null && Chess.board[r][2] == null && Chess.board[r][3] == null){
                    Chess.board[r][3] = Chess.board[r][0];
                    Chess.board[r][0] = null;

                    Chess.board[r][2].setRow(r);
                    Chess.board[r][2].setCol(5);

                    this.hasMoved = true;
                    return true;
                }
            }
        }
        // check if space is already occupied by piece of the same team
        if(Chess.board[r][c] != null && this.getTeam() == Chess.board[r][c].getTeam()){
            return false;
        }

        if (r == row + 1){
            if (c == col +1 || c == col -1 || c == col){
                this.hasMoved = true;
                return true;
            }
        }
        else if (r == row){
            if (c == col +1 || c == col -1){
                this.hasMoved = true;
                return true;
            }
        }
        else if (r == row -1){
            if (c == col +1 || c == col -1 || c == col) {
                this.hasMoved = true;
                return true;
            }
        }
        else{
            return false;
        }
        return false;
    }
}
