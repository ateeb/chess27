package chess;

import piece.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Manages the game of chess.
 *
 * @author Syed Ateeb Jamal and Daanyal Farooqi
 */
public class Chess {

    /** Represents the game board as a 2D array of Piece.
     */
    public static Piece[][] board = new Piece[8][8];
    /** Represents the white king piece.
     */
    public static Piece whiteKing;
    /**
     * Represents the black king piece.
     */
    public static Piece blackKing;

    /**
     * Method that begins execution and runs the chess game. All inputs and outputs through the command line.
     *
     * @param args command line movement inputs
     */
    public static void main(String[] args){
        generateBoard();

        boolean complete = false;
        boolean whiteTurn = true;
        boolean drawOffered = false;
        String result = "no result";
        Scanner scanner = new Scanner(System.in);

        while(!complete){
            printBoard();

            //Prompt for turns
            if(whiteTurn){
                if(isCheck(whiteKing.psnString(), whiteKing.psnString(), Piece.Team.WHITE)){
                    if (isCheckmate(Piece.Team.WHITE)){
                        System.out.println("Checkmate");
                        result = "Black wins";
                        complete = true;
                        break;
                    }
                    else{
                    System.out.println("Check");}
                }
                System.out.print("White's move: ");
            }
            else{
                if(isCheck(blackKing.psnString(), blackKing.psnString(), Piece.Team.BLACK)){
                    if (isCheckmate(Piece.Team.BLACK)){
                        System.out.println("Checkmate");
                        result = "White wins";
                        complete = true;
                        break;
                    }
                    else{
                        System.out.println("Check");}
                }
                System.out.print("Black's move: ");
            }

            String command = scanner.nextLine();
            System.out.println();   //yes this has to be here and not earlier or later
            command = command.trim();
            String[] input = command.split("\\s+");
            //splitting so that index 0 and index 1 hold start and end place, respectively

            //Check for draw accepted
            if(drawOffered && command.toLowerCase().contains("draw")){
                result = "Draw";
                complete = true;
                break;
            }
            else if(drawOffered){
                drawOffered = false;
            }

            //Offer draw
            if(command.toLowerCase().contains("draw?")){
                drawOffered = true;
            }

            //Check for resign
            if(command.toLowerCase().contains("resign")){
                if(whiteTurn){
                    result = "Black wins";
                }
                else{
                    result = "White wins";
                }
                complete = true;
                break;
            }

            //Process move
            if(getRow(input[0]) == -1 || getCol(input[0]) == -1 || getRow(input[1]) == -1 || getCol(input[1]) == -1){
                System.out.println("Illegal move, try again");
                continue;
            }

            Piece piece = board[getRow(input[0])][getCol(input[0])];
            //System.out.println(piece.allPossibleMoves());

            if(piece == null){
                System.out.println("Illegal move, try again");
                continue;
            }
            else if(piece.getTeam() == Piece.Team.WHITE && whiteTurn){
                if(piece.isLegalMove(input[1]) && !isCheck(input[0], input[1], Piece.Team.WHITE)){
                    piece.setRow(getRow(input[1]));
                    piece.setCol(getCol(input[1]));
                    board[getRow(input[1])][getCol(input[1])] = piece;
                    board[getRow(input[0])][getCol(input[0])] = null;

                    //Check for pawn promotion
                    if(piece instanceof Pawn && piece.getRow() == 0){
                        if(input.length < 3){
                            board[piece.getRow()][piece.getCol()] = new Queen(piece.getRow(), piece.getCol(), "WHITE");
                        }
                        else{
                            Piece promoted;

                            switch (input[2].toUpperCase()){
                                case "R": promoted = new Rook(piece.getRow(), piece.getCol(), "WHITE"); break;
                                case "N": promoted = new Knight(piece.getRow(), piece.getCol(), "WHITE"); break;
                                case "B": promoted = new Bishop(piece.getRow(), piece.getCol(), "WHITE"); break;
                                default: promoted = new Queen(piece.getRow(), piece.getCol(), "WHITE");
                            }

                            board[piece.getRow()][piece.getCol()] = promoted;
                        }
                    }
                }
                else{
                    System.out.println("Illegal move, try again");
                    continue;
                }
            }
            else if(piece.getTeam() == Piece.Team.BLACK && !whiteTurn){
                if(piece.isLegalMove(input[1]) && !isCheck(input[0], input[1], Piece.Team.BLACK)){
                    piece.setRow(getRow(input[1]));
                    piece.setCol(getCol(input[1]));
                    board[getRow(input[1])][getCol(input[1])] = piece;
                    board[getRow(input[0])][getCol(input[0])] = null;

                    //Check for pawn promotion
                    if(piece instanceof Pawn && piece.getRow() == 7){
                        if(input.length < 3){
                            board[piece.getRow()][piece.getCol()] = new Queen(piece.getRow(), piece.getCol(), "BLACK");
                        }
                        else{
                            Piece promoted;

                            switch (input[2].toUpperCase()){
                                case "R": promoted = new Rook(piece.getRow(), piece.getCol(), "BLACK"); break;
                                case "N": promoted = new Knight(piece.getRow(), piece.getCol(), "BLACK"); break;
                                case "B": promoted = new Bishop(piece.getRow(), piece.getCol(), "BLACK"); break;
                                default: promoted = new Queen(piece.getRow(), piece.getCol(), "BLACK");
                            }

                            board[piece.getRow()][piece.getCol()] = promoted;
                        }
                    }
                }
                else{
                    System.out.println("Illegal move, try again");
                    continue;
                }
            }
            else{
                System.out.println("Illegal move, try again");
                continue;
            }

            //I think this makes sense
            if(Pawn.passant != piece){
                Pawn.passant = null;
            }

            whiteTurn = !whiteTurn;
        }

        System.out.println(result);
    }

    /**
     * Checks if the specified team is in checkmate.
     *
     * @param myteam which team to check the check mate for
     * @return whether or not the specified team is in checkmate
     */
    public static boolean isCheckmate(Piece.Team myteam){
        //Clone board
        Piece[][] copy = new Piece[8][];
        for(int i = 0; i < 8; i++)
            copy[i] = board[i].clone();

        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++) {
                if(copy[i][j] != null && copy[i][j].team == myteam){
                    ArrayList<String> allMoves = copy[i][j].allPossibleMoves();
                    for (String s : allMoves){
                        if(!isCheck(copy[i][j].psnString(),s,copy[i][j].getTeam())){
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * Checks if the specified team is in check after a specified move.
     *
     * @param oldPsn The current position of the piece you wish to move and check for a check after.
     * @param newPsn The new position of the piece at oldPsn.
     * @param myteam The team of which you are checking the check status of
     * @return Whether the turn puts the specified team in a check.
     */
    public static boolean isCheck(String oldPsn, String newPsn, Piece.Team myteam) {
        int row = getRow(oldPsn);
        int col = getCol(oldPsn);

        int r = getRow(newPsn);
        int c = getCol(newPsn);

        String kingPsn;

        //Clone board
        Piece[][] copy = new Piece[8][];
        for(int i = 0; i < 8; i++)
            copy[i] = board[i].clone();

        copy[r][c] = copy[row][col];
        copy[row][col] = null;

        if(myteam == Piece.Team.WHITE){
            if(copy[r][c] instanceof King && copy[r][c].getTeam() == Piece.Team.WHITE){
                kingPsn = newPsn;
            }
            else {
                kingPsn = whiteKing.psnString();
            }

            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    if(copy[i][j] != null && copy[i][j].getTeam() == Piece.Team.BLACK){
                        Piece black = copy[i][j];
                        if(black.isLegalMove(kingPsn)){
                            return true;
                        }
                    }
                }
            }
        }
        else{
            if(copy[r][c] instanceof King && copy[r][c].getTeam() == Piece.Team.BLACK){
                kingPsn = newPsn;
            }
            else {
                kingPsn = blackKing.psnString();
            }

            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    if(copy[i][j] != null && copy[i][j].getTeam() == Piece.Team.WHITE){
                        Piece white = copy[i][j];
                        if(white.isLegalMove(kingPsn)){
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Parses the input based on the UI format into array indexes as readable by the code.
     * @param psn The string input representing the position of a piece.
     * @return The row index of a piece in board[][]
     */
    public static int getRow(String psn){
        int r = Integer.parseInt(psn.charAt(1) + "");
        r = 8-r;
        if(! (0<=r && r<=7)){
            return -1;
        }
        else{
            return r;
        }
    }

    /**
     * Parses the input based on the UI format into array indexes as readable by the code.
     * @param psn The string input representing the position of a piece.
     * @return The column index of a piece in board[][]
     */
    public static int getCol(String psn){
        char c = psn.charAt(0);

        switch(c){
            case 'a': return 0;
            case 'b': return 1;
            case 'c': return 2;
            case 'd': return 3;
            case 'e': return 4;
            case 'f': return 5;
            case 'g': return 6;
            case 'h': return 7;
            default: return -1;
        }
    }

    /**
     * Creates a fresh board of chess with all pieces in their initial positions in board[][]
     */
    public static void generateBoard(){

        //Pawns
        for(int i = 0; i<8; i++){
            board[6][i] = new Pawn(6, i, "WHITE");
            board[1][i] = new Pawn(1, i, "BLACK");
        }

        //Rooks
        board[7][0] = new Rook(7, 0, "WHITE");
        board[7][7] = new Rook(7, 7, "WHITE");
        board[0][0] = new Rook(0,0, "BLACK");
        board[0][7] = new Rook(0,7, "BLACK");

        //Knights
        board[7][1] = new Knight(7, 1, "WHITE");
        board[7][6] = new Knight(7, 6, "WHITE");
        board[0][1] = new Knight(0,1, "BLACK");
        board[0][6] = new Knight(0,6, "BLACK");

        //Bishop
        board[7][2] = new Bishop(7, 2, "WHITE");
        board[7][5] = new Bishop(7, 5, "WHITE");
        board[0][2] = new Bishop(0,2, "BLACK");
        board[0][5] = new Bishop(0,5, "BLACK");

        //King & Queen
        whiteKing = new King(7, 4, "WHITE");
        blackKing = new King(0,4, "BLACK");

        board[7][4] = whiteKing;
        //board[7][4] = new King(7, 4, "WHITE");
        board[7][3] = new Queen(7, 3, "WHITE");
        board[0][4] = blackKing;
        //board[0][4] = new King(0,4, "BLACK");
        board[0][3] = new Queen(0,3, "BLACK");
    }

    /**
     * Prints out the board in its current state from board[][]
     */
    public static void printBoard(){
        for(int i = 0; i<8; i++){
            for(int j = 0; j<8; j++){
                //draw grid on empty spaces using math
                if(board[i][j] == null){
                    if((i%2==0 && j%2 != 0) || (i%2!=0 && j%2==0)){
                        System.out.print("## ");
                    }
                    else {
                        System.out.print("   ");
                    }
                }
                else {
                    System.out.print(board[i][j].toString() + " ");
                }
            }
            System.out.println("" + (8-i));
        }
        System.out.println(" a  b  c  d  e  f  g  h");
        System.out.println();
    }
}
